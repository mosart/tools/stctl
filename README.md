# STCTL

## Dependencies

The tool was written in [`Python3`](https://www.python.org/downloads/) and the
dependencies can be installed using the following command.

```
pip3 install -r requirements.txt
```

If the parser and lexer files need to be generated, you can run the following
command in the `src` folder, once you have installed [antlr](https://www.antlr.org/).

```
antlr -Dlanguage=Python3 -no-listener -visitor -o dist Imitator.g4
antlr -Dlanguage=Python3 -no-listener -visitor -o dist STCTL.g4
```

## Getting Started

### Generating use case models

The script `generator.py` allows generating two models: `philosophers` and
`voters`.

For **philosophers**, where `n` is the number of philosophers :
```bash
python3 generator.py --n N philo
```

For **voters**, where `n` is the number of voters, and `c` is the number of candidates.
```bash
python3 generator.py --n N voter -c C
```

### Generating model with strategies

```bash
➜ python app.py --help
usage: app.py [-h] [--output OUTPUT] --model MODEL --property PROPERTY --method {parameters,variables} --synthesis {one,all}

STCTL

options:
  -h, --help            show this help message and exit
  --output OUTPUT       output file (default: ./output.imi)
  --model MODEL         Imitator file (default: None)
  --property PROPERTY   Property file (default: None)
  --method {parameters,variables}
                        Transformation method (default: None)
  --synthesis {one,all}
                        Sythesis method (default: None)
```

## Running Test

A set of tests can be run with the following command:

```
python3 -m pytest -vvs
```
