import argparse
import os

from src.Translator import (
    FormulaTranslator,
    ModelTranslator,
    ParametersStrategy,
    VariablesStrategy,
)


def main(
    model_file: str,
    property_file: str,
    method: str,
    synthesis: str,
    output_filename: str,
):
    """
    Translate an IMITATOR file and a property

    Parameters
    ----------
    model_file : str
        Imitator filename
    property_file : str
        Property filename
    method : str
        Transformation method
    synthesis : str
        Synthesis one or all strategies
    output_filename : str
        Path of the output file
    """
    try:
        model_translator = ModelTranslator()
        if method == "parameters":
            output = model_translator.run(
                model_file, property_file, ParametersStrategy()
            )
        elif method == "variables":
            output = model_translator.run(
                model_file, property_file, VariablesStrategy()
            )
        else:
            raise RuntimeError(f"Method {method} is not supported.")

        formula_translator = FormulaTranslator()
        output_f = formula_translator.run(property_file, synthesis)
        output_formula_filename = os.path.join(
            os.path.dirname(output_filename),
            os.path.basename(property_file).replace(".stctl", f"_{synthesis}.imiprop"),
        )

        # write to file
        with open(output_filename, "w", encoding="utf8") as model_f, open(
            output_formula_filename, "w", encoding="utf8"
        ) as formula_f:
            model_f.write(output)
            formula_f.write(output_f)
            print(
                f"Output files:\n  - {output_filename}\n  - {output_formula_filename}"
            )
    except Exception as main_e:
        print(f"Error processing the files.\n {main_e}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""STCTL""", formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "--output", type=str, default="./output.imi", help="output file"
    )

    parser.add_argument("--model", type=str, required=True, help="Imitator file")
    parser.add_argument("--property", type=str, required=True, help="Property file")
    parser.add_argument(
        "--method",
        type=str,
        required=True,
        choices=["parameters", "variables"],
        help="Transformation method",
    )
    parser.add_argument(
        "--synthesis",
        type=str,
        required=True,
        choices=["one", "all"],
        help="Sythesis method",
    )

    args = parser.parse_args()
    main(args.model, args.property, args.method, args.synthesis, args.output)
