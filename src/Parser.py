import antlr4

from src.dist.ImitatorLexer import ImitatorLexer
from src.dist.ImitatorParser import ImitatorParser
from src.dist.STCTLLexer import STCTLLexer
from src.dist.STCTLParser import STCTLParser
from src.Imitator import Model
from src.ImitatorVisitor import ImitatorVisitor
from src.STCTL import Formula
from src.STCTLVisitor import STCTLVisitor


class Parser:
    """Class encapsulating the parsers for models and formulas"""

    def parse_model(self, filename: str) -> Model:
        """
        Parse an Imitator file

        Parameters
        ----------
        filename : str
            Imitator file

        Returns
        -------
        Model
        """
        fs_in = antlr4.FileStream(filename, encoding="utf-8")

        # lexer
        lexer = ImitatorLexer(fs_in)
        stream = antlr4.CommonTokenStream(lexer)

        # parser
        parser = ImitatorParser(stream)
        tree = parser.main()

        # visitor
        visitor = ImitatorVisitor()
        model = visitor.visit(tree)

        return model

    def parse_formula(self, filename: str) -> Formula:
        """
        Parse an STCTL formula file

        Parameters
        ----------
        filename : str
            formula file

        Returns
        -------
        STCTL
        """
        fs_in = antlr4.FileStream(filename, encoding="utf-8")

        # lexer
        lexer = STCTLLexer(fs_in)
        stream = antlr4.CommonTokenStream(lexer)

        parser = STCTLParser(stream)
        tree = parser.main()

        # visitor
        visitor = STCTLVisitor()
        formula = visitor.visit(tree)

        return formula
