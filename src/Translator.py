from abc import ABC, abstractmethod

from src.Imitator import Automaton, Model
from src.Parser import Parser
from src.STCTL import And, Atom, Formula, Interval, Not, StateFormula


class Strategy(ABC):
    """
    Abstract classs representing a strategy for generating a new Imitator
    model with strategies
    """

    def get_invariant(self, interval: Interval, clk: str) -> str:
        """Translates an constraint interval into a location invariant

        Parameters
        ----------
        interval : Interval
            temporal constraint
        clk : str
            clock used in the invariant

        Returns
        -------
        str
        """
        if interval is None:
            return "t>=0"

        # lower bound
        lower_closed = "=" if interval.is_left_close() else ""
        constraint = f"{clk}>{lower_closed}{interval.lower}"

        # upper bound
        if not interval.is_infinity():
            upper_closed = "=" if interval.is_right_close() else ""
            constraint += f"&{clk}<{upper_closed}{interval.upper}"

        return constraint

    @abstractmethod
    def translate(self, model: Model, formula: Formula) -> Model:
        """Method to translate an Imitator model

        Parameters
        ----------
        model : Model
            Imitator model
        formula : Formula
            STCTL formula

        Returns
        -------
        Model
        """


class ModelTranslator:
    """
    Class encapsulating the translator of an Imitator model into a one
    handling strategies

    Parameters
    ----------
    parser : Parser
        parser for Imitator models and STCTL formulas
    """

    def __init__(self) -> None:
        """Constructor of the class"""
        self.parser = Parser()

    def run(self, model_file: str, formula_file: str, strategy: Strategy) -> str:
        """Return a model handling strategies by applying a transformation
        strategy on an imitator model and a stctl formula.

        Parameters
        ----------
        model_file : str
            path to an Imitator model
        formula_file : str
            path to an STCTL formula
        strategy : Strategy
            strategy to be applied

        Returns
        -------
        str
        """
        model = self.parser.parse_model(model_file)
        formula = self.parser.parse_formula(formula_file)
        output = strategy.translate(model, formula)
        return output.to_imitator()


class FormulaTranslator:
    """
    Class encapsulating the translator of a STCTL formula into a formula handled
    by Imitator.

    Parameters
    ----------
    parser : Parser
        parser for Imitator models and STCTL formulas
    """

    def __init__(self) -> None:
        """Constructor of the class"""
        self.parser = Parser()

    def get_stctl_formula(self, formula: StateFormula) -> str:
        """Parses a stctl state formula into the syntax handled by Imitator

        Parameters
        ----------
        formula : StateFormula
            STCTL state formula

        Returns
        -------
        str
        """
        if isinstance(formula, Atom):
            return f"loc[{formula.agent}] = {formula.label}"
        if isinstance(formula, Not):
            return f"not ({self.get_stctl_formula(formula.child)})"
        if isinstance(formula, And):
            return f"{self.get_stctl_formula(formula.lchild)} & {self.get_stctl_formula(formula.rchild)}"

        raise RuntimeError(f"Constructor {formula} is not supported.")

    def run(self, formula_file: str, mode: str) -> str:
        """Return a formula in a syntax handled by Imitator

        Parameters
        ----------
        formula_file : str
            path to an STCTL formula
        mode : synthesis mode
            sythesis mode in Imitator. It can be "one" or "all" the strategies.

        Returns
        -------
        str
        """
        formula = self.parser.parse_formula(formula_file)
        inner_formula = self.get_stctl_formula(formula.formula)

        full_formula = f"property := {{mode}} {formula.quantifier}({inner_formula});"

        if mode == "one":
            return full_formula.format(mode="#witness")
        if mode == "all":
            return full_formula.format(mode="#synth")
        raise RuntimeError(f"Synthesis {mode} is not supported.")


class ParametersStrategy(Strategy):
    """Class encapsulating the strategy for translating a model using parameters"""

    def translate(self, model: Model, formula: Formula) -> Model:
        """Returns a model using the transformation with paramaters

        Parameters
        ----------
        model : Model
            Imitator model
        formula : Formula
            STCTL formula

        Returns
        -------
        Model
        """
        # get the automata that are in the formula
        automata = [model.get_automaton(a) for a in formula.agents]

        # formula has temporal constraint
        if formula.interval is not None:
            # add a new clock
            model.add_clock("t")
            # add invariant to the formula states
            inv = self.get_invariant(formula.interval, "t")
            for a, loc in formula.formula.values():
                model.get_automaton(a).get_location(loc).add_invariant(inv)

        # apply the transformation to each automaton
        for a_index, a in enumerate(automata):
            queue = [a.initial_location.name]
            visited = []
            while queue:
                current_s = queue.pop(0)
                visited.append(current_s)

                next_transitions = a.transitions_from(current_s)
                nb_transitions = len(next_transitions)

                # add a new parameter to the model
                if nb_transitions > 1:
                    new_p_name = f"p_{next_transitions[0].source}_{a_index+1}"
                    model.add_parameter(new_p_name)
                    model.add_initial_constraint(f"{new_p_name}>=0")

                for t_index, t in enumerate(next_transitions):
                    # add next state to the queue if not already visited
                    if t.target not in visited and t.target not in queue:
                        queue.append(t.target)

                    # take and uptate transition with parameter
                    if nb_transitions > 1:
                        new_p = f"{new_p_name}={t_index + 1}"
                        t.guard = new_p if t.guard == "True" else f"{t.guard} & {new_p}"

        return model


class VariablesStrategy(Strategy):
    """Class encapsulating the strategy for translating a model using discrete variables"""

    def max_nb_transitions(self, automaton: Automaton) -> int:
        """Return the maximum number of outgoing transitions of states in an automaton

        Parameters
        ----------
        automaton : Automaton
            automaton to be analyzed

        Returns
        -------
        int
        """
        queue = [automaton.initial_location.name]
        visited = []
        max_n = 0
        while queue:
            current_s = queue.pop(0)
            visited.append(current_s)

            next_transitions = automaton.transitions_from(current_s)
            max_n = max(max_n, len(next_transitions))

            for t in next_transitions:
                # add next state to the queue if not already visited
                if t.target not in visited and t.target not in queue:
                    queue.append(t.target)

        return max_n

    def int2bin(self, number: int, length: int) -> str:
        """Convert an integer in binary with a specific length

        Parameters
        ----------
        number : int
            integer value
        length : int
            lenght of the output string

        Returns
        -------
        str
        """
        return f"0b{number:0{length}b}"

    def get_guard(self, variable: str, value: int, length: int) -> str:
        """Returns the string of the new guard of a transition using a binary representation

        Parameters
        ----------
        variable : str
            variable to be tested in the guard
        value : int
            value to be compared in the guard
        length : int
            lenght of the binary type

        Returns
        -------
        str
        """
        return f"({variable} = {self.int2bin(0, length)} | {variable} = {self.int2bin(value, length)})"

    def get_update(self, variable: str, value: int, length: int) -> str:
        """Returns the string of the new updates of a transition using a binary representation

        Parameters
        ----------
        variable : str
            variable to be updated
        value : int
            new value of the variable
        length : int
            length of the binary type

        Returns
        -------
        str
        """
        return f"if {variable} = {self.int2bin(0, length)} then {variable} := {self.int2bin(value, length)} end"

    def translate(self, model: Model, formula: Formula) -> Model:
        """Returns a model using the transformation with discrete variables

        Parameters
        ----------
        model : Model
            Imitator model
        formula : Formula
            STCTL formula

        Returns
        -------
        Model
        """
        # get the automata that are in the formula
        automata = [model.get_automaton(a) for a in formula.agents]

        # apply the transformation to each automaton
        for a_index, a in enumerate(automata):
            bin_len = len(bin(self.max_nb_transitions(a))) - 2
            zero_bin = self.int2bin(0, bin_len)

            queue = [a.initial_location.name]
            visited = []
            while queue:
                current_s = queue.pop(0)
                visited.append(current_s)

                next_transitions = a.transitions_from(current_s)
                nb_transitions = len(next_transitions)

                # add a new parameter to the model
                if nb_transitions > 1:
                    new_var_name = f"var_{next_transitions[0].source}_{a_index+1}"
                    model.add_discrete_var(new_var_name, f"binary({bin_len})")
                    model.add_initial_constraint(f"{new_var_name} := {zero_bin}")

                for t_index, t in enumerate(next_transitions):
                    # add next state to the queue if not already visited
                    if t.target not in visited and t.target not in queue:
                        queue.append(t.target)

                    # take and update transition with new value for the corresponding variable
                    if nb_transitions > 1:
                        new_var = self.get_guard(new_var_name, t_index + 1, bin_len)
                        t.guard = (
                            new_var if t.guard == "True" else f"{t.guard} & {new_var}"
                        )
                        new_update = self.get_update(new_var_name, t_index + 1, bin_len)
                        t.add_update(new_update)
        return model
