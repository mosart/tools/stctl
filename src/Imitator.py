"""Imitator Model

This module contains the classes to represent an Imitator model
"""
import textwrap
from collections import defaultdict
from itertools import filterfalse, tee
from typing import Union


class Transition:
    """
    A class to represent a transition of an automaton

    Attributes
    ----------
    source : str
        Identifier of the source state
    guard : str
        The guard of the transition
    update : list[str]
        The updates of the transition
    sync : str
        The synchronization label of the transition
    target : str
        Identifier of the target state
    """

    def __init__(
        self, source: str, guard: str, update: list[str], sync: str, target: str
    ):
        """
        Constructor of the class

        Parameters
        ----------
        source : str
            Identifier of the source state
        guard : str
            The guard of the transition
        update : list[str]
            The updates of the transition
        sync : str
            The synchronization label of the transition
        target : str
            Identifier of the target state
        """
        self.source = str(source) if source is not None else None
        self.guard = str(guard) if guard is not None else None
        self.update = list(update) if update is not None else []
        self.sync = str(sync) if sync is not None else None
        self.target = str(target) if target is not None else None

    def to_imitator(self) -> str:
        """Returns the string representation of a transition with the Imitator syntax

        Returns
        -------
        str
        """
        transition_str = f"when {self.guard}"

        if self.sync is not None:
            transition_str += f"\n    sync {self.sync} "

        updates_str = f'do {{{", ".join(self.update)}}} ' if self.update else ""
        transition_str += f"{updates_str}goto {self.target};"
        return transition_str

    def add_update(self, new_update: str):
        """Add a new update to the list of updates

        Parameters
        ----------
        new_update : str
            new update
        """
        self.update.append(new_update)

    def __str__(self):
        return f"Transition({self.source} |-[{self.guard}, {self.update}, {self.sync})]-> {self.target}"

    def __repr__(self):
        return str(self)


class Location:
    """
    A class to represent a location of an automaton

    Attributes
    ----------
    name : str
        location's name
    invariant : str
        locations's invariant
    initial : bool
        flag to mark a location as an initial location
    accepting : bool
        flag to mark a location as an accepting location
    urgent : bool
        flag to mark a location as an urgent location
    """

    def __init__(
        self, name: str, invariant: str, initial: bool, accepting: bool, urgent: bool
    ):
        """
        Constructor of the class

        Parameters
        ----------
        name : str
            location's name
        invariant : str
            locations's invariant
        initial : bool
            flag to mark a location as an initial location
        accepting : bool
            flag to mark a location as an accepting location
        urgent : bool
            flag to mark a location as an urgent location
        """
        self.name = str(name)
        self.invariant = str(invariant)
        self.initial = bool(initial)
        self.accepting = bool(accepting)
        self.urgent = bool(urgent)

    def to_imitator(self, transitions: list[Transition]) -> str:
        """Return the string representation of a location with the Imitator syntax

        Parameters
        ----------
        transitions : list[Transition]
            list of the outgoing tranistions of the location

        Returns
        -------
        str
        """
        accepting = "accepting " if self.accepting else ""
        urgent = "urgent " if self.urgent else ""

        location_str = (
            f"\n\n{accepting}{urgent}loc {self.name}: invariant {self.invariant}"
        )

        # parse transitions
        for t in transitions:
            location_str += "\n  " + t.to_imitator()

        return location_str

    def add_invariant(self, invariant: str):
        """Add an invariant to the location

        Parameters
        ----------
        invariant : str
            invariant to be added
        """
        if self.invariant == "True":
            self.invariant = invariant
        else:
            self.invariant += f"& {invariant}"

    def __str__(self):
        marks = []
        if self.initial:
            marks.append("->")
        if self.accepting:
            marks.append("*")
        if self.urgent:
            marks.append("!")

        marks_str = "[" + ",".join(marks) + "]" if marks else ""
        return f"Location{marks_str} {self.name} ({self.invariant})"

    def __repr__(self):
        return str(self)


class Automaton:
    """
    A class to represent an automaton

    Attributes
    ----------
    name : str
        name of the automaton
    actions : list[str]
        actions of the automaton
    locations :  list[Location]
        locations of the automaton
    transitions : list[Transition]
        transitions of the automaton
    initial_location : Location
        initial location of the automaton
    accepting_locations : list[Location]
        accepting locations of the automaton
    urgent_locations : list[Location]
        urgent locations of the automaton
    """

    def __init__(self, name: str, actions: list[str]):
        """
        Constructor of the class

        Parameters
        ----------
        name : str
            name of the automaton
        actions : list[str]
            synchronisation labels
        """
        self.name = str(name)
        self.actions = list(actions)
        self._locations = defaultdict()
        self._transitions = defaultdict(list)

    def add_location(self, location: Location):
        """
        Add a new location to the automaton.

        Parameters
        ----------
        location : Location
            the new location
        """
        self._locations[location.name] = location

    def add_transition(self, transition: Transition):
        """
        Add a new transition to the automaton.

        Parameters
        ----------
        transition : Transition
            the new transition
        """
        self._transitions[transition.source].append(transition)

    @property
    def locations(self):
        """Return the locations of the automaton

        Returns
        -------
        list[Location]
        """
        return list(self._locations.values())

    @property
    def transitions(self):
        """Return the transitions of the automaton

        Returns
        -------
        list[Transition]
        """
        return [t for t_list in self._transitions.values() for t in t_list]

    @property
    def initial_location(self):
        """Return the initial location of the automaton

        Returns
        -------
        Location
        """
        return next((loc for loc in self.locations if loc.initial), None)

    @property
    def accepting_locations(self):
        """Return the accepting locations of the automaton

        Returns
        -------
        list[Location]
        """
        return [loc for loc in self.locations if loc.accepting]

    @property
    def urgent_locations(self):
        """Return the urgent locations of the automaton

        Returns
        -------
        list[Location]
        """
        return [loc for loc in self.locations if loc.urgent]

    def get_location(self, name: str) -> Location:
        """
        Get a location by its name.

        Parameters
        ----------
        name : str
            locations's name

        Returns
        -------
        Location
        """
        return self._locations[name]

    def transitions_from(self, source: str) -> list[Transition]:
        """Get transitions from a given location.

        Parameters
        ----------
        source : str
            Location'name

        Returns
        -------
        list[Transition]
            list of transitions
        """
        return list(self._transitions[source])

    def sync_transitions(self, action: str) -> list[Transition]:
        """Get transitions synchronized by an action"""
        return [t for t in self.transitions if t.sync == action]

    def get_all_labels(self) -> list[str]:
        "Get the labels from all the transitions"
        return list({t.sync for t in self.transitions})

    def to_imitator(self) -> str:
        """Return the string representation of the automaton in the Imitator syntax

        Returns
        -------
        str
        """
        name = self.name

        automaton_str = textwrap.dedent(
            f"""
            (************************************************************)
            automaton {name}
            (************************************************************)
            """
        ).strip()

        # parse actions
        if self.actions:
            synclabs_str = ", ".join(sorted(self.actions))
            automaton_str += f"\nsynclabs: {synclabs_str};"

        # parse locations
        for loc in self.locations:
            automaton_str += loc.to_imitator(self.transitions_from(loc.name))

        return f"{automaton_str}\nend (* {name} *)"

    def __str__(self):
        return f"Automaton({self.name}, {self.actions}, {self.locations})"

    def __repr__(self):
        return str(self)


class Model:
    """
    Class representing an Imitator model

    Attributes
    ----------
    automata: list[Automaton]
        List of automata composing the model
    parameters: list[str]
        List of parameters of the model
    clocks: list[str]
        List of clocks of the model
    actions: list[str]
        List of actions of the model
    initial_constraints : list[str]
        List of initial parameter constraints
    """

    def __init__(self):
        """Constructor of the class"""
        self.automata = []
        self.parameters = []
        self.clocks = []
        self.discrete_vars = []
        self.actions = []
        self.initial_constraints = []

    def add_parameter(self, param: Union[str, list[str]]):
        """
        Add a parameter or a list of parameters to the model

        Parameters
        ----------
        param : list[str] or str
            a parameter or a list of parameters to be added
        """
        if isinstance(param, list):
            self.parameters.extend(param)
        else:
            self.parameters.append(param)

        # remove repeated values
        self.parameters = list(set(self.parameters))

    def add_clock(self, clock: Union[str, list[str]]):
        """
        Add a clock or a list of clocks to the model

        Parameters
        ----------
        clock : list[str] or str
            a clock or a list of clocks to be added
        """
        if isinstance(clock, list):
            self.clocks.extend(clock)
        else:
            self.clocks.append(clock)

        # remove repeated values
        self.clocks = list(set(self.clocks))

    def add_discrete_var(self, var: Union[str, list[str]], var_type: str):
        """
        Add a discrete variable or a list of discrete variable to the model

        Parameters
        ----------
        var : list[str] or str
            a discrete variable or a list of discrete variables to be added
        var_type : str
            type of the discrete variable
        """
        if isinstance(var, list):
            self.discrete_vars.extend([(v, var_type) for v in var])
        else:
            self.discrete_vars.append((var, var_type))

    def add_automaton(self, automaton: Union[Automaton, list[Automaton]]):
        """
        Add an automaton or a list of automata to the model

        Parameters
        ----------
        automaton : list[Automaton] or Automaton
            an automaton or a list of automata to be added
        """
        if isinstance(automaton, list):
            self.automata.extend(automaton)
            self.actions.extend([actions for a in automaton for actions in a.actions])
        else:
            self.automata.append(automaton)
            self.actions.extend(automaton.actions)
        self.actions = list(set(self.actions))

    def add_initial_constraint(self, constraint: Union[str, list[str]]):
        """
        Add an initial constraint or a list of initial constraints to the model

        Parameters
        ----------
        constraint : list[str] or str
            a constraint or a list of constraints to be added
        """
        if isinstance(constraint, list):
            self.initial_constraints.extend(constraint)
        else:
            self.initial_constraints.append(constraint)

    def get_automaton(self, name: str) -> Automaton:
        """
        Get an automaton by its name

        Parameters
        ----------
        name : str
            automaton's name

        Returns
        -------
        Automaton
        """
        return next((a for a in self.automata if a.name == name), None)

    def get_variable_by_type(self) -> list[tuple[str, list[str]]]:
        """Return the list of discrete variables grouped by its type

        Returns
        -------
        list
        """
        result = {}
        for k, v in self.discrete_vars:
            result.setdefault(v, []).append(k)
        return list(result.items())

    def partition(self, pred: callable, list_: list) -> tuple[list, list]:
        """Use a predicate to partition entries into false entries and true entries"

        Parameters
        ----------
        pred : callable
            predicate to be tested on each entry
        list_ : list
            list to be partitioned

        Returns
        -------
        (list, list)
        """
        t1, t2 = tee(list_)
        return list(filterfalse(pred, t1)), list(filter(pred, t2))

    def to_imitator(self) -> str:
        """
        Parse to Imitator syntax

        Returns
        -------
        str
        """
        imitator_str = "var"

        # parse clocks, discrete varibles and parameters
        if self.clocks:
            clocks_str = ", ".join(sorted(self.clocks))
            imitator_str += f"\n  {clocks_str} : clock;"
        if self.discrete_vars:
            for var_type, variables in self.get_variable_by_type():
                discrete_var_str = ", ".join(variables)
                imitator_str += f"\n  {discrete_var_str} : {var_type};"
        if self.parameters:
            parameters_str = ", ".join(sorted(self.parameters))
            imitator_str += f"\n  {parameters_str} : parameter;"

        # parse automata
        automata_str = ""
        for automaton in self.automata:
            automata_str += "\n\n" + automaton.to_imitator()
        imitator_str += automata_str + "\n\n"

        continuous, discrete = self.partition(
            lambda x: ":=" in x, self.initial_constraints
        )

        initial_constraints_str = "\n                ".join(
            [f"& {c}" for c in continuous]
        )

        all_discrete = [
            f"loc[{a.name}] := {a.initial_location.name}" for a in self.automata
        ] + discrete
        initial_locations_str = ",\n                ".join(all_discrete)

        imitator_str += textwrap.dedent(
            f"""
        (************************************************************)
        (* Initial state *)
        (************************************************************)

        init := {{
            discrete =
                {initial_locations_str}
            ;

            continuous =
                {initial_constraints_str}
            ;
        }}

        (************************************************************)
        (* The end *)
        (************************************************************)
        end
        """
        ).strip()

        return imitator_str

    def __str__(self):
        return f"clocks: {self.clocks}\nparameters: {self.parameters}\ndiscrete: {self.discrete_vars}\nactions: {self.actions}\nautomata: {self.automata}"

    def __repr__(self):
        return str(self)
