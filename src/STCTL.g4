/**
 * This file defines the syntax for parsing STCTL formulas
 */
grammar STCTL;

// ------------------------------------------------------------------------
//  Parser Rules
// ------------------------------------------------------------------------

// Main
main          : LBRA LBRA agent_list? RBRA RBRA path_formula EOF;

// agents
agent_list    : NAME (COMMA NAME)*;

// formula
formula       : agent=NAME DOT atom=NAME                #Atom
              | NOT formula                             #Negation
              | left=formula CONJUNCTION right=formula  #Condition
              ;

// formula
path_formula  : REACHABILITY interval? LPAR formula RPAR #EF
              | SAFETY interval? LPAR formula RPAR #AG
              ;

// temporal constraint
interval      : LPAR lower=NAT COMMA upper=natinf RPAR     #BothOpen
              | LSQBRA lower=NAT COMMA upper=natinf RPAR   #OpenRight
              | LPAR lower=NAT COMMA upper=natinf RSQBRA   #OpenLeft
              | LSQBRA lower=NAT COMMA upper=natinf RSQBRA #BothClose
              ;

natinf        : NAT | INF;

// ------------------------------------------------------------------------
// Lexer Rules
// ------------------------------------------------------------------------

// operators
NOT           : '~';
CONJUNCTION   : '^';

// path operators
SAFETY        : 'AG';
REACHABILITY  : 'EF';

// symbols
DOT           : '.';
LBRA          : '<';
RBRA          : '>';
COMMA         : ',';
LPAR          : '(';
RPAR          : ')';
LSQBRA        : '[';
RSQBRA        : ']';

// numbers
NAT           : [0-9]+;
INF           : 'inf';

// names
NAME          : '_'* [A-Za-z][A-Za-z0-9_]*;

// comments & new lines
COMMENT       : '(*' .*? '*)' -> skip;
NEWLINE       : ('\r'? '\n' | '\r')+ -> skip;
WHITESPACE    : (' ' | '\t')+ -> skip;