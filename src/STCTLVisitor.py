from src.dist.STCTLParser import STCTLParser
from src.dist.STCTLVisitor import STCTLVisitor
from src.STCTL import AG, EF, And, Atom, Interval, Not, StateFormula


class STCTLVisitor(STCTLVisitor):
    """
    Class extending the visitor of STCTL formulas

    Attributes
    ----------
    agents : list[str]
        list of agents involved in the strategy
    formula : Formula
        STCTL formula
    """

    def __init__(self):
        super().__init__()
        self.agents = []
        self.formula = None

    def visitMain(self, ctx):
        # parse list of agents
        if ctx.agent_list() is not None:
            self.agents = ctx.agent_list().getText().split(",")

        # parse formula
        self.formula = self.visitPath_formula(ctx.path_formula())

        return self.formula

    def visitFormula(self, ctx) -> StateFormula:
        ctx_type = type(ctx)
        if ctx_type == STCTLParser.AtomContext:  # atom
            return Atom(ctx.agent.text, ctx.atom.text)
        if ctx_type == STCTLParser.NegationContext:  # negation
            return Not(self.visitFormula(ctx.formula()))
        if ctx_type == STCTLParser.ConditionContext:  # conjuntion
            left = self.visitFormula(ctx.left)
            right = self.visitFormula(ctx.right)
            return And(left, right)
        return None

    def visitPath_formula(self, ctx):
        ctx_type = type(ctx)
        formula = self.visitFormula(ctx.formula())

        interval = ctx.interval()
        if interval is not None:
            if interval.getText().startswith("[0,inf"):
                # default value
                interval = None
            else:
                lower = interval.lower.text
                upper = interval.natinf().getText()
                bounds = interval.getText()[0] + interval.getText()[-1]
                interval = Interval(lower, upper, bounds)

        if ctx_type == STCTLParser.AGContext:
            return AG(formula, self.agents, interval)
        if ctx_type == STCTLParser.EFContext:
            return EF(formula, self.agents, interval)
        return None
