from src.dist.ImitatorVisitor import ImitatorVisitor
from src.Imitator import Automaton, Location, Model, Transition


class ImitatorVisitor(ImitatorVisitor):
    """
    Class extending the visitor of Imitator models

    Attributes
    ----------
    model : Model
        Imitator model
    """

    def __init__(self):
        super().__init__()
        self.model = Model()

    def visitMain(self, ctx):
        self.visitChildren(ctx)
        return self.model

    def visitClock_definitions(self, ctx):
        clocks = ctx.name_list().getText().split(",")
        clocks = [c for c in clocks if len(c)]
        self.model.add_clock(clocks)

        return self.visitChildren(ctx)

    def visitDiscrete_definition(self, ctx):
        variables = ctx.name_list().getText().split(",")
        variables = [v for v in variables if len(v)]
        self.model.add_discrete_var(variables, "discrete")

        return self.visitChildren(ctx)

    def visitParam_definition(self, ctx):
        parameters = ctx.const_name_list().getText().split(",")
        parameters = [p for p in parameters if len(p)]
        for p in parameters:
            if "=" in p:
                self.model.add_initial_constraint(p)
                p = p.split("=")[0]
            self.model.add_parameter(p)

        return self.visitChildren(ctx)

    def visitAutomaton(self, ctx):
        synclabs = []
        parsed_synclabs = ctx.synclabs()
        if parsed_synclabs is not None and parsed_synclabs.name_list() is not None:
            synclabs = parsed_synclabs.name_list().getText().split(",")

        automaton = Automaton(ctx.NAME(), synclabs)
        locations = ctx.locations()

        # first parse locations
        for location in locations:
            invariant = location.invariant().predicate().getText()
            source = location.NAME()

            # parse types of the location
            location_types = []
            if location.location_type() is not None:
                location_types = [
                    loc_t.getText() for loc_t in location.location_type().getChildren()
                ]

            accepting = "accepting" in location_types
            urgent = "urgent" in location_types

            automaton.add_location(
                Location(source, invariant, False, accepting, urgent)
            )

        # first parse transitions
        for location in locations:
            source = location.NAME()
            for transition in location.transition():
                guard = transition.predicate().getText()
                target = transition.NAME()

                action = transition.action()
                update = []
                sync = None
                if action is not None:
                    if action.updates() is not None:
                        update_list = action.updates().update_list()
                        if update_list is not None:
                            update = update_list.getText().split(",")

                    sync = action.sync_label()
                    if sync is not None:
                        sync = str(action.sync_label().NAME())

                automaton.add_transition(
                    Transition(source, guard, update, sync, target)
                )

        self.model.add_automaton(automaton)
        return self.visitChildren(ctx)

    def visitInitial_location(self, ctx):
        automaton = str(ctx.auto.text)
        location = ctx.loc.text
        self.model.get_automaton(automaton).get_location(location).initial = True

        return self.visitChildren(ctx)

    def visitInitial_constraint(self, ctx):
        constraints = ctx.expr().getText().split("&")
        self.model.add_initial_constraint(constraints)

        return self.visitChildren(ctx)
