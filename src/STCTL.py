"""STCTL formula

This module contains the classes to represent an STCTL formula
"""


class Interval:
    """Interval representing the interval of the quantifier formula"""

    def __init__(self, lower: int, upper: str, bounds: str):
        """Constructor of the class

        Parameters
        ----------
        lower : int
            lower bound of the interval
        upper :str
            upper bound of the interval. It's a natural or inf
        bounds : str
            string with the left and right bounds. The possibilites are : (), (], [), []
        """
        if bounds not in ["()", "(]", "[)", "[]"]:
            raise RuntimeError(f"{bounds} is not supported by the parser")

        self.lower = int(lower)
        self.upper = "inf" if upper.startswith("inf") else int(upper)
        self.bounds = bounds

    def is_left_open(self):
        """Return if the lower bound is open, i.e. ("""
        return self.bounds[0] == "("

    def is_right_open(self):
        """Return if the upper bound is open, i.e. )"""
        return self.bounds[1] == ")"

    def is_left_close(self):
        """Return if the lower bound is closed, i.e. ["""
        return self.bounds[0] == "["

    def is_right_close(self):
        """Return if the upper bound is closed, i.e. ]"""
        return self.bounds[1] == "]"

    def is_infinity(self):
        """Return if the interval is unbounded, i.e. the upper bound is inf"""
        return isinstance(self.upper, str) and self.upper.lower().startswith("inf")

    def __str__(self):
        return f"{self.bounds[0]}{self.lower},{self.upper}{self.bounds[1]}"

    __repr__ = __str__


class StateFormula:
    """interface representing a state formula"""

    def values(self):
        pass

    def __str__(self):
        pass

    __repr__ = __str__


class Formula:
    """A class to represent a STCTL formula

    Attributes
    ----------
    formula : StateFormula
        state formula
    agents : list[str]
        list of agent's involded in the strategy
    quantifier : str
        path quantifier of the formula
    """

    def __init__(
        self, formula: StateFormula, agents: list[str], interval: Interval = None
    ):
        """Constructor of the class

        Parameters
        ----------
        formula : StateFormula
            state formula
        agents : list[str]
            list of agent's involded in the strategy
        interval : Interval
            temporal constraint on the quantifir formula
        """
        self.agents = agents
        self.formula = formula
        self.quantifier = ""
        self.interval = interval

    def __str__(self):
        interval = f"{str(self.interval)} " if self.interval is not None else ""
        return f'<<{", ".join(self.agents)}>> {self.quantifier} {interval}({str(self.formula)})'

    __repr__ = __str__


class EF(Formula):
    """Class representing a reachability formula

    Attributes
    ----------
    formula : StateFormula
        state formula
    agents : list[str]
        list of agent's involded in the strategy
    quantifier : str
        path quantifier of the formula
    interval : Interval
        temporal constraint on the quantifir formula
    """

    def __init__(self, formula: StateFormula, agents: list[str], interval: Interval):
        """Constructor of the class

        Parameters
        ----------
        formula : StateFormula
            state formula
        agents : list[str]
            list of agent's involded in the strategy
        """
        super().__init__(formula, agents, interval)
        self.quantifier = "EF"


class AG(Formula):
    """Class representing a safety formula

    Attributes
    ----------
    formula : StateFormula
        state formula
    agents : list[str]
        list of agent's involded in the strategy
    quantifier : str
        path quantifier of the formula
    interval : Interval
        temporal constraint on the quantifir formula
    """

    def __init__(self, formula: StateFormula, agents: list[str], interval: Interval):
        """Constructor of the class

        Parameters
        ----------
        formula : StateFormula
            state formula
        agents : list[str]
            list of agent's involded in the strategy
        """
        super().__init__(formula, agents, interval)
        self.quantifier = "AG"


class Atom(StateFormula):
    """Class representing a proposition true for an agent

    Attributes
    ----------
    agent: str
        agent
    label : str
        label of the atom
    """

    def __init__(self, agent: str, label: str):
        """Constructor of the class

        Parameters
        ----------
        agent: str
            agent
        label : str
            label of the atom
        """
        self.agent = agent
        self.label = label

    def values(self):
        """Returns the values of the atom"""
        return [(self.agent, self.label)]

    def __str__(self):
        return f"loc[{self.agent}]={str(self.label)}"


class Not(StateFormula):
    """Class representing the negation operator

    Attributes
    ----------
    child : StateFormula
        formula that is negated
    """

    def __init__(self, child: StateFormula):
        """Constructor of the class

        Parameters
        ----------
        child : StateFormula
            formula that is negated
        """
        self.child = child

    def values(self):
        """Returns the values of the negation"""
        return self.child.values()

    def __str__(self):
        return "~" + str(self.child)


class And(StateFormula):
    """Class representing the conjunction of two formulas

    Attributes
    ----------
    lchild : StateFormula
        left clause of the conjunction
    rchild : StateFormula
        right clause of the conjuntion
    """

    def __init__(self, lchild: StateFormula, rchild: StateFormula):
        """Constructor of the class

        Parameters
        ----------
        lchild : StateFormula
            left clause of the conjunction
        rchild : StateFormula
            right clause of the conjuntion
        """
        self.lchild = lchild
        self.rchild = rchild

    def values(self):
        """Returns the values of the conjunction"""
        return self.lchild.values() + self.rchild.values()

    def __str__(self):
        return "(" + str(self.lchild) + " " + "^" + " " + str(self.rchild) + ")"
