import argparse
import os

from src.Imitator import Automaton, Location, Model, Transition


class VoterGenerator:
    """Class encapsulating the logic to generate the voters model"""

    def __init__(self):
        """Constructor of the class"""
        self.voting_methods = ["m", "i", "p"]

    def generate(self, n: int, n_candidates: int) -> Model:
        """Generate the Imitator model for the voters model

        Parameters
        ----------
        n : int
            number of voters
        n_candidates : int
            number of candidates

        Returns
        -------
        Model
            Imitator model
        """
        model = Model()

        # authority
        authority = self.authority(n, n_candidates)
        model.add_automaton(authority)

        # add clocks
        clocks = ["x", "t"]
        model.add_clock(clocks)
        model.add_initial_constraint([f"{c} >= 0" for c in clocks])

        # voters
        voters = [self.voter(i, n_candidates) for i in range(1, n + 1)]
        model.add_automaton(voters)

        return model

    def authority(self, n: int, n_candidates: int) -> Automaton:
        """Generate the model for the authority

        Parameters
        ----------
        n : int
            number of voters
        n_candidates : int
            number of candidates

        Returns
        -------
        Automaton
            authority automaton
        """
        clock_t = "t"
        clock_x = "x"
        authority_automaton = Automaton("authority", [])

        voters = range(1, n + 1)
        candidates = range(1, n_candidates + 1)

        # initial location
        authority_automaton.add_location(
            Location("a0", f"{clock_t} <= 11", True, False, False)
        )

        guards = {
            "m": [f"{clock_t} <= 1", f"1 <= {clock_t} & {clock_t} <= 7"],
            "i": [f"{clock_t} <= 6", f"6 <= {clock_t} & {clock_t} <= 9"],
            "p": [f"{clock_t} <= 10", f"10 <= {clock_t} & {clock_t} <= 11"],
        }

        for v in voters:
            for m in self.voting_methods:
                authority_automaton.add_location(
                    Location(f"{m}{v}", f"{clock_x} <= 0", False, False, False)
                )

                # add transition for sending the voting
                authority_automaton.add_transition(
                    Transition(f"{m}{v}", f"{clock_x} = 0", [], f"pack_{m}_{v}", "a0")
                )

                # add transition for registration
                authority_automaton.add_transition(
                    Transition(
                        "a0",
                        guards[m][0],
                        [f"{clock_x} := 0"],
                        f"reg_{m}_{v}",
                        f"{m}{v}",
                    )
                )

                # add transitions for receiving votes
                for c in candidates:
                    authority_automaton.add_transition(
                        Transition(
                            "a0",
                            guards[m][1],
                            [],
                            f"vote{v}_{m}_{c}",
                            "a0",
                        )
                    )

        # closed location
        authority_automaton.add_location(Location("closed", True, False, False, False))
        authority_automaton.add_transition(
            Transition("a0", f"{clock_t} = 11", [], "close", "closed")
        )

        authority_automaton.actions = authority_automaton.get_all_labels()

        return authority_automaton

    def voter(self, i: int, n_candidates: int) -> Automaton:
        """Generate the model for a ith-voter

        Parameters
        ----------
        i : int
            identifier of the voter
        n_candidates : int
            number of candidates

        Returns
        -------
        Automaton
            automaton of the voter
        """
        candidates = range(1, n_candidates + 1)
        voter_automaton = Automaton(f"voter{i}", [])

        # initial location
        voter_automaton.add_location(Location(f"v{i}0", True, True, False, False))

        for m in self.voting_methods:
            # locations in depth 2
            voter_automaton.add_location(Location(f"{m}{i}", True, False, False, False))

            # locations in depth 3
            voter_automaton.add_location(
                Location(f"r{m}{i}", True, False, False, False)
            )

        # locations in depth 4
        for c in candidates:
            voter_automaton.add_location(
                Location(f"voted{i}{c}", True, False, False, False)
            )

        for m in self.voting_methods:
            # transitions in depth 1
            voter_automaton.add_transition(
                Transition(f"v{i}0", True, [], f"reg_{m}_{i}", f"{m}{i}")
            )

            # transitions in depth 2
            voter_automaton.add_transition(
                Transition(f"{m}{i}", True, [], f"pack_{m}_{i}", f"r{m}{i}")
            )

            # transitions in depth 3
            for c in candidates:
                voter_automaton.add_transition(
                    Transition(f"r{m}{i}", True, [], f"vote{i}_{m}_{c}", f"voted{i}{c}")
                )

        voter_automaton.actions = voter_automaton.get_all_labels()

        return voter_automaton


class PhilosopherGenerator:
    """Class encapsulating the logic to generate the philosopher model"""

    def generate(self, n: int) -> Model:
        """Generate an PTA model for the philosopher model

        Parameters
        ----------
        n : int
            Number of philosophers

        Returns
        -------
        Model
            Imitator model
        """
        model = Model()

        # add clocks
        clocks = [f"x{i}" for i in range(0, n)]
        model.add_clock(clocks)

        # philosphers
        philosophers = [self.philosopher(i) for i in range(0, n)]
        model.add_automaton(philosophers)

        # forks
        fork = [self.fork(i, n) for i in range(0, n)]
        model.add_automaton(fork)

        # lackey
        lackey = self.lackey(n)
        model.add_automaton(lackey)

        # initial constraints on clocks
        init_clocks = [f"{c} >= 0" for c in clocks]
        model.add_initial_constraint(init_clocks)

        return model

    def philosopher(self, i: int) -> Automaton:
        """Generate the automaton for the ith-philopher

        Parameters
        ----------
        i : int
            identifier of the philosopher. i >= 0

        Returns
        -------
        Automaton
            Automaton of the philosopher
        """
        clock = f"x{i}"
        reset_clock = f"{clock} := 0"

        # automaton
        philo = Automaton(f"philo{i}", [])

        # locations
        philo.add_location(
            Location(f"thinking{i}", f"{clock} <= 2", True, False, False)
        )
        philo.add_location(Location(f"hungry{i}", True, False, False, False))
        philo.add_location(Location(f"waiting{i}", True, False, False, False))
        philo.add_location(Location(f"waitingR{i}", True, False, False, False))
        philo.add_location(Location(f"waitingL{i}", True, False, False, False))
        philo.add_location(Location(f"eating{i}", f"{clock} <= 1", False, False, False))
        philo.add_location(Location(f"releaseR{i}", True, False, False, False))
        philo.add_location(Location(f"releaseL{i}", True, False, False, False))
        philo.add_location(Location(f"released{i}", True, False, False, False))

        # transitions
        philo.add_transition(
            Transition(f"thinking{i}", f"{clock} >= 1", [], f"wantEat{i}", f"hungry{i}")
        )
        philo.add_transition(
            Transition(f"hungry{i}", True, [], f"in{i}", f"waiting{i}")
        )
        philo.add_transition(
            Transition(f"waiting{i}", True, [], f"getR{i}", f"waitingL{i}")
        )
        philo.add_transition(
            Transition(f"waiting{i}", True, [], f"getL{i}", f"waitingR{i}")
        )
        philo.add_transition(
            Transition(f"waitingL{i}", True, [reset_clock], f"getL{i}", f"eating{i}")
        )
        philo.add_transition(
            Transition(f"waitingR{i}", True, [reset_clock], f"getR{i}", f"eating{i}")
        )
        philo.add_transition(
            Transition(f"eating{i}", f"{clock} >= 5", [], f"putR{i}", f"releaseR{i}")
        )
        philo.add_transition(
            Transition(f"eating{i}", f"{clock} >= 5", [], f"putL{i}", f"releaseL{i}")
        )
        philo.add_transition(
            Transition(f"releaseR{i}", f"{clock} >= 5", [], f"putL{i}", f"released{i}")
        )
        philo.add_transition(
            Transition(f"releaseL{i}", f"{clock} >= 5", [], f"putR{i}", f"released{i}")
        )
        philo.add_transition(
            Transition(f"released{i}", True, [reset_clock], f"out{i}", f"thinking{i}")
        )

        philo.actions = philo.get_all_labels()

        return philo

    def fork(self, i: int, n: int) -> Automaton:
        """Generate the automaton of the ith-fork

        Parameters
        ----------
        i : int
            identifier of the fork. i>=0
        n : int
            number of philophers. n >= 1

        Returns
        -------
        Automaton
            automaton of the fork
        """
        i_mod_n = (i + 1) % n

        # automaton
        fork_automaton = Automaton(f"fork{i}", [])

        # locations
        fork_automaton.add_location(Location(f"idle{i}", True, True, False, False))
        fork_automaton.add_location(Location(f"use{i}", True, False, False, False))

        # transitions
        fork_automaton.add_transition(
            Transition(f"idle{i}", True, [], f"getL{i}", f"use{i}")
        )
        fork_automaton.add_transition(
            Transition(f"idle{i}", True, [], f"getR{i_mod_n}", f"use{i}")
        )
        fork_automaton.add_transition(
            Transition(f"use{i}", True, [], f"putL{i}", f"idle{i}")
        )
        fork_automaton.add_transition(
            Transition(f"use{i}", True, [], f"putR{i_mod_n}", f"idle{i}")
        )

        fork_automaton.actions = fork_automaton.get_all_labels()

        return fork_automaton

    def lackey(self, n: int) -> Automaton:
        """Generate the automaton for lackey

        Parameters
        ----------
        n : int
            number of philosophers

        Returns
        -------
        Automaton
            automaton for lackey
        """

        # automaton
        lackey_automaton = Automaton("lackey", [])

        # locations
        for i in range(0, n):
            lackey_automaton.add_location(Location(f"l{i}", True, i == 0, False, False))

        # transitions
        for i in range(1, n):
            for j in range(0, n):
                # in transition
                lackey_automaton.add_transition(
                    Transition(f"l{i-1}", True, [], f"in{j}", f"l{i}")
                )
                # out transition
                lackey_automaton.add_transition(
                    Transition(f"l{i}", True, [], f"out{j}", f"l{i-1}")
                )

        lackey_automaton.actions = lackey_automaton.get_all_labels()

        return lackey_automaton


def main(code: str, filename: str, output_folder: str) -> None:
    """Main function for the CLI

    Parameters
    ----------
    code : str
        content to be store
    filename : str
        name of the generated file
    output_folder : str
        path of the output folder
    """
    output_file = os.path.join(output_folder, f"{filename}.imi")
    with open(output_file, "w", encoding="utf8") as f:
        f.write(code)
        print(f"Output file: {output_file}")


def generate_philo(args_philo: argparse.Namespace) -> None:
    """Generate the philosophers imitator code

    Parameters
    ----------
    args : argparse.Namespace
        arguments from the CLI
    """
    filename = f"{args_philo.model}_n{args_philo.n}"
    output = PhilosopherGenerator().generate(args_philo.n).to_imitator()
    main(output, filename, args_philo.output_folder)


def generate_voter(args_voters: argparse.Namespace) -> None:
    """Generate the voters imitator code

    Parameters
    ----------
    args_voters : argparse.Namespace
        arguments from the CLI
    """
    filename = f"{args_voters.model}_n{args_voters.n}_c{args_voters.c}"
    output = VoterGenerator().generate(args_voters.n, args_voters.c).to_imitator()
    main(output, filename, args_voters.output_folder)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generator of models",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    subparser = parser.add_subparsers(title="generators", dest="model", required=True)

    parser.add_argument("--n", type=int, required=True, help="number of agents")
    parser.add_argument("--output_folder", type=str, default=".", help="output folder")

    parser_philo = subparser.add_parser("philo", help="generate the philosopher model")
    parser_philo.set_defaults(func=generate_philo)

    parser_voter = subparser.add_parser("voter", help="generate the voter model")
    parser_voter.set_defaults(func=generate_voter)
    parser_voter.add_argument(
        "--c", type=int, required=True, help="number of candidates"
    )

    args = parser.parse_args()
    args.func(args)
