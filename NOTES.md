# Notes

## Tool

There will three versions of the tool:

- **Using parameters:** This version is the one used in the conference paper. It
  uses parameters to model strategies, and conditions on the transitions. Then, it
  uses imitator to synthesize the parameters.
- **Using variables:** This version uses variables to model strategies. It sets
  variables on transitions when a strategy is taken. In order to avoid modifying
  the value of variables, conditions are used on transitions. Then, imitator is
  used to explore the state space.
- **Using counting (Future):** The idea is to use queues in order to record the
  number of times a transition is taken (counting).

The tool takes as input

- a model (IMITATOR model), and
- a STCTL formula (no-nested CTL formula: EF or AGnot)

The tool returns:

- An IMITATOR model
- Whether the model satisfies the formula (Yes/No)
- Synthesize 1 strategy
- Synthesize all strategies

![whiteboard](docs/whiteboard.jpeg)
![whiteboard2](docs/whiteboard-2.jpeg)

## Benchmarks

We will present three benchmarks

- Voting Model
- Railway crossing system with a malicious intruder [AKPP16]
- Philosophers

## Conferences

This implementation will be used for :

- Journal version of the AAMAS 2023 paper
- AAMAS 2024
- TACAS 2024
