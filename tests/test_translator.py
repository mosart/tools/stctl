import os

import pytest

from src.Translator import (
    FormulaTranslator,
    ModelTranslator,
    ParametersStrategy,
    VariablesStrategy,
)


def remove_spaces(output_str):
    """Removes all the spaces in a string"""
    output = list(filter(None, [e.strip() for e in output_str.split("\n")]))
    return output


@pytest.mark.parametrize(
    "model_file,formula_file,output_file",
    [
        ("voters.imi", "1-agent.stctl", "voters_params_1.imi"),
        ("philo.imi", "even-philo.stctl", "philo_params.imi"),
    ],
)
def test_parser_param(examples_dir, model_file, formula_file, output_file):
    translator = ModelTranslator()

    # parse the imitator file
    model = os.path.join(examples_dir, model_file)
    formula = os.path.join(examples_dir, formula_file)
    output = translator.run(model, formula, ParametersStrategy())

    # read the file with the expected output
    expected_file = os.path.join(examples_dir, output_file)
    expected = open(expected_file).read()

    assert remove_spaces(output) == remove_spaces(expected)


@pytest.mark.parametrize(
    "model_file,formula_file,output_file",
    [
        ("voters.imi", "1-agent.stctl", "voters_variables_1.imi"),
        ("philo.imi", "even-philo.stctl", "philo_variables.imi"),
    ],
)
def test_parser_var(examples_dir, model_file, formula_file, output_file):
    translator = ModelTranslator()

    # parse the imitator file
    model = os.path.join(examples_dir, model_file)
    formula = os.path.join(examples_dir, formula_file)
    output = translator.run(model, formula, VariablesStrategy())

    # read the file with the expected output
    expected_file = os.path.join(examples_dir, output_file)
    expected = open(expected_file).read()

    assert remove_spaces(output) == remove_spaces(expected)


@pytest.mark.parametrize(
    "formula_file,output_file,synthesis",
    [
        ("1-agent.stctl", "1-agent_all.imiprop", "all"),
        ("1-agent.stctl", "1-agent_one.imiprop", "one"),
        ("even-philo.stctl", "even-philo_all.imiprop", "all"),
        ("even-philo.stctl", "even-philo_one.imiprop", "one"),
    ],
)
def test_parser_formula(examples_dir, formula_file, output_file, synthesis):
    translator = FormulaTranslator()

    # parse the formula file
    formula = os.path.join(examples_dir, formula_file)
    output = translator.run(formula, synthesis)

    # read the file with the expected output
    expected_file = os.path.join(examples_dir, output_file)
    expected = open(expected_file).read()

    assert remove_spaces(output) == remove_spaces(expected)
