var
  x0, x1, x2 : clock;
  var_l0_1, var_l1_1, var_l2_1 : binary(3);
  E1, E2, T1, T2 : parameter;

(************************************************************)
automaton philo0
(************************************************************)
synclabs: getL0, getR0, in0, out0, putL0, putR0, wantEat0;

loc thinking0: invariant x0<=T1
  when x0>=T2
    sync wantEat0 goto hungry0;

loc hungry0: invariant True
  when True
    sync in0 goto waiting0;

loc waiting0: invariant True
  when True
    sync getR0 goto waitingL0;
  when True
    sync getL0 goto waitingR0;

loc waitingR0: invariant True
  when True
    sync getR0 do {x0:=0} goto eating0;

loc waitingL0: invariant True
  when True
    sync getL0 do {x0:=0} goto eating0;

loc eating0: invariant x0<=E1
  when x0>=E2
    sync putR0 goto releaseR0;
  when x0>=E2
    sync putL0 goto releaseL0;

loc releaseR0: invariant True
  when x0>=E2
    sync putL0 goto released0;

loc releaseL0: invariant True
  when x0>=E2
    sync putR0 goto released0;

loc released0: invariant True
  when True
    sync out0 do {x0:=0} goto thinking0;
end (* philo0 *)

(************************************************************)
automaton philo1
(************************************************************)
synclabs: getL1, getR1, in1, out1, putL1, putR1, wantEat1;

loc thinking1: invariant x1<=T1
  when x1>=T2
    sync wantEat1 goto hungry1;

loc hungry1: invariant True
  when True
    sync in1 goto waiting1;

loc waiting1: invariant True
  when True
    sync getR1 goto waitingL1;
  when True
    sync getL1 goto waitingR1;

loc waitingR1: invariant True
  when True
    sync getR1 do {x1:=0} goto eating1;

loc waitingL1: invariant True
  when True
    sync getL1 do {x1:=0} goto eating1;

loc eating1: invariant x1<=E1
  when x1>=E2
    sync putR1 goto releaseR1;
  when x1>=E2
    sync putL1 goto releaseL1;

loc releaseR1: invariant True
  when x1>=E2
    sync putL1 goto released1;

loc releaseL1: invariant True
  when x1>=E2
    sync putR1 goto released1;

loc released1: invariant True
  when True
    sync out1 do {x1:=0} goto thinking1;
end (* philo1 *)

(************************************************************)
automaton philo2
(************************************************************)
synclabs: getL2, getR2, in2, out2, putL2, putR2, wantEat2;

loc thinking2: invariant x2<=T1
  when x2>=T2
    sync wantEat2 goto hungry2;

loc hungry2: invariant True
  when True
    sync in2 goto waiting2;

loc waiting2: invariant True
  when True
    sync getR2 goto waitingL2;
  when True
    sync getL2 goto waitingR2;

loc waitingR2: invariant True
  when True
    sync getR2 do {x2:=0} goto eating2;

loc waitingL2: invariant True
  when True
    sync getL2 do {x2:=0} goto eating2;

loc eating2: invariant x2<=E1
  when x2>=E2
    sync putR2 goto releaseR2;
  when x2>=E2
    sync putL2 goto releaseL2;

loc releaseR2: invariant True
  when x2>=E2
    sync putL2 goto released2;

loc releaseL2: invariant True
  when x2>=E2
    sync putR2 goto released2;

loc released2: invariant True
  when True
    sync out2 do {x2:=0} goto thinking2;
end (* philo2 *)

(************************************************************)
automaton fork0
(************************************************************)
synclabs: getL0, getR1, putL0, putR1;

loc idle0: invariant True
  when True
    sync getL0 goto use0;
  when True
    sync getR1 goto use0;

loc use0: invariant True
  when True
    sync putL0 goto idle0;
  when True
    sync putR1 goto idle0;
end (* fork0 *)

(************************************************************)
automaton fork1
(************************************************************)
synclabs: getL1, getR2, putL1, putR2;

loc idle1: invariant True
  when True
    sync getL1 goto use1;
  when True
    sync getR2 goto use1;

loc use1: invariant True
  when True
    sync putL1 goto idle1;
  when True
    sync putR2 goto idle1;
end (* fork1 *)

(************************************************************)
automaton fork2
(************************************************************)
synclabs: getL2, getR0, putL2, putR0;

loc idle2: invariant True
  when True
    sync getL2 goto use2;
  when True
    sync getR0 goto use2;

loc use2: invariant True
  when True
    sync putL2 goto idle2;
  when True
    sync putR0 goto idle2;
end (* fork2 *)

(************************************************************)
automaton lackey
(************************************************************)
synclabs: in0, in1, in2, out0, out1, out2;

loc l0: invariant True
  when (var_l0_1 = 0b000 | var_l0_1 = 0b001)
    sync in0 do {if var_l0_1 = 0b000 then var_l0_1 := 0b001 end} goto l1;
  when (var_l0_1 = 0b000 | var_l0_1 = 0b010)
    sync in1 do {if var_l0_1 = 0b000 then var_l0_1 := 0b010 end} goto l1;
  when (var_l0_1 = 0b000 | var_l0_1 = 0b011)
    sync in2 do {if var_l0_1 = 0b000 then var_l0_1 := 0b011 end} goto l1;

loc l1: invariant True
  when (var_l1_1 = 0b000 | var_l1_1 = 0b001)
    sync out0 do {if var_l1_1 = 0b000 then var_l1_1 := 0b001 end} goto l0;
  when (var_l1_1 = 0b000 | var_l1_1 = 0b010)
    sync out1 do {if var_l1_1 = 0b000 then var_l1_1 := 0b010 end} goto l0;
  when (var_l1_1 = 0b000 | var_l1_1 = 0b011)
    sync out2 do {if var_l1_1 = 0b000 then var_l1_1 := 0b011 end} goto l0;
  when (var_l1_1 = 0b000 | var_l1_1 = 0b100)
    sync in0 do {if var_l1_1 = 0b000 then var_l1_1 := 0b100 end} goto l2;
  when (var_l1_1 = 0b000 | var_l1_1 = 0b101)
    sync in1 do {if var_l1_1 = 0b000 then var_l1_1 := 0b101 end} goto l2;
  when (var_l1_1 = 0b000 | var_l1_1 = 0b110)
    sync in2 do {if var_l1_1 = 0b000 then var_l1_1 := 0b110 end} goto l2;

loc l2: invariant True
  when (var_l2_1 = 0b000 | var_l2_1 = 0b001)
    sync out0 do {if var_l2_1 = 0b000 then var_l2_1 := 0b001 end} goto l1;
  when (var_l2_1 = 0b000 | var_l2_1 = 0b010)
    sync out1 do {if var_l2_1 = 0b000 then var_l2_1 := 0b010 end} goto l1;
  when (var_l2_1 = 0b000 | var_l2_1 = 0b011)
    sync out2 do {if var_l2_1 = 0b000 then var_l2_1 := 0b011 end} goto l1;
end (* lackey *)

(************************************************************)
(* Initial state *)
(************************************************************)

init := {
    discrete =
        loc[philo0] := thinking0,
        loc[philo1] := thinking1,
        loc[philo2] := thinking2,
        loc[fork0] := idle0,
        loc[fork1] := idle1,
        loc[fork2] := idle2,
        loc[lackey] := l0,
        var_l0_1 := 0b000,
        var_l1_1 := 0b000,
        var_l2_1 := 0b000
    ;

    continuous =
        & x0>=0
        & x1>=0
        & x2>=0
        & T1>=0
        & T2>=0
        & E1>=0
        & E2>=0
    ;
}

(************************************************************)
(* The end *)
(************************************************************)
end