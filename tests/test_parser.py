import os

from src.Parser import Parser


def all_elements(l1, l2):
    assert set(l1) == set(l2)


def test_parser_model(examples_dir):
    parser = Parser()

    # parse the imitator file
    input_file = os.path.join(examples_dir, "voters.imi")
    model = parser.parse_model(input_file)

    # check clocks
    all_elements(model.clocks, ["t", "x"])
    # check parameters
    all_elements(model.parameters, [])
    # check discrete values
    all_elements(model.discrete_vars, [])
    # check automata
    all_elements([a.name for a in model.automata], ["authority", "voter1", "voter2"])
    # check initial constraints
    all_elements(model.initial_constraints, ["x>=0", "t=0"])

    model_actions = [
        "reg_m_1",
        "reg_i_1",
        "reg_p_1",
        "pack_m_1",
        "pack_i_1",
        "pack_p_1",
        "vote1_m_1",
        "vote1_i_1",
        "vote1_p_1",
        "vote2_m_1",
        "vote2_i_1",
        "vote2_p_1",
        "reg_m_2",
        "reg_i_2",
        "reg_p_2",
        "pack_m_2",
        "pack_i_2",
        "pack_p_2",
        "vote1_m_2",
        "vote1_i_2",
        "vote1_p_2",
        "vote2_m_2",
        "vote2_i_2",
        "vote2_p_2",
        "close",
    ]

    # check all actions
    all_elements(model.actions, model_actions)

    # check action from authority automaton
    authority = model.get_automaton("authority")
    assert authority.name == "authority"
    assert authority.initial_location.name == "a0"
    all_elements(authority.actions, model_actions)
    all_elements(
        [loc.name for loc in authority.locations],
        ["a0", "m1", "i1", "p1", "m2", "i2", "p2", "closed"],
    )
    all_elements(authority.accepting_locations, [])
    all_elements(authority.urgent_locations, [])

    voter1 = model.get_automaton("voter1")
    assert voter1.name == "voter1"
    assert voter1.initial_location.name == "v10"
    all_elements(
        voter1.actions,
        [
            "reg_m_1",
            "reg_i_1",
            "reg_p_1",
            "pack_m_1",
            "pack_i_1",
            "pack_p_1",
            "vote1_m_1",
            "vote1_i_1",
            "vote1_p_1",
            "vote1_m_2",
            "vote1_i_2",
            "vote1_p_2",
        ],
    )
    all_elements(
        [loc.name for loc in voter1.locations],
        ["v10", "m1", "i1", "p1", "rm1", "ri1", "rp1", "voted11", "voted12"],
    )
    all_elements(voter1.accepting_locations, [])
    all_elements(voter1.urgent_locations, [])

    voter2 = model.get_automaton("voter2")
    assert voter2.name == "voter2"
    assert voter2.initial_location.name == "v20"
    all_elements(
        voter2.actions,
        [
            "reg_m_2",
            "reg_i_2",
            "reg_p_2",
            "pack_m_2",
            "pack_i_2",
            "pack_p_2",
            "vote2_m_1",
            "vote2_i_1",
            "vote2_p_1",
            "vote2_m_2",
            "vote2_i_2",
            "vote2_p_2",
        ],
    )
    all_elements(
        [loc.name for loc in voter2.locations],
        ["v20", "m2", "i2", "p2", "rm2", "ri2", "rp2", "voted21", "voted22"],
    )
    all_elements(voter2.accepting_locations, [])
    all_elements(voter2.urgent_locations, [])

    # check location without transitions
    assert len(voter2.transitions_from("voted21")) == 0

    # check location with transitions without updates
    location_rp2 = voter2.get_location("rp2")
    rp2_t = voter2.transitions_from("rp2")[0]
    assert not location_rp2.accepting
    assert not location_rp2.urgent
    assert not location_rp2.initial
    assert location_rp2.invariant == "True"
    assert location_rp2.name == "rp2"
    assert rp2_t.guard == "True"
    assert rp2_t.source == "rp2"
    assert rp2_t.target == "voted21"
    assert rp2_t.sync == "vote2_p_1"
    assert len(rp2_t.update) == 0

    # check location with transitions with updates
    location_a0 = authority.get_location("a0")
    a0_t = authority.transitions_from("a0")[0]
    assert location_a0.invariant == "t<=11"
    assert a0_t.guard == "t<1"
    assert a0_t.source == "a0"
    assert a0_t.target == "m1"
    assert a0_t.sync == "reg_m_1"
    all_elements(a0_t.update, ["x:=0"])


def test_parser_formula(examples_dir):
    parser = Parser()

    # parse the imitator file
    input_file = os.path.join(examples_dir, "1-agent.stctl")
    formula = parser.parse_formula(input_file)

    assert len(formula.agents) == 1
    assert formula.agents[0] == "voter1"

    assert formula.quantifier == "EF"
    assert formula.formula.label == "voted11"

    assert formula.interval is None


def test_parser_formula_interval(examples_dir):
    parser = Parser()

    # parse the imitator file
    input_file = os.path.join(examples_dir, "1-agent_interval.stctl")
    formula = parser.parse_formula(input_file)

    assert len(formula.agents) == 1
    assert formula.agents[0] == "voter1"

    assert formula.quantifier == "EF"
    assert formula.formula.label == "voted11"

    assert formula.interval.is_left_open()
    assert formula.interval.is_right_open()
    assert formula.interval.lower == 0
    assert formula.interval.upper == "inf"
    assert formula.interval.is_infinity()
